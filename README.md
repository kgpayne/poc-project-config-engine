# poc-project-config-engine

An attempt at re-imagining multi-yaml project config.

## Spec

Ideally we would have a top-level `ProjectConfig` interface for interacting with project config files, abstracting away where the actual config is defined.

Persistence would be handeled by a reader-writer (possibly the [datafiles yaml ORM](https://github.com/jacebrowning/datafiles)), with an index of entities and files managed by the top-level interface.

## Jupyter Notes

A notebook is provided for playing with the POC. Jupyter notebook or labs supports multiple kernels. Therefore you need only have jupyter (`pip3 install jupyterlab`) installed once against you main python environment (ideally not system - use `pyenv`) and then in this project dir run:

```bash
poetry install
poetry run python -m ipykernel install --user --name project_config_engine
```

to install an `ipykernel` inside a poetry-managed `venv` and add the new kernel to your list of kernels. Finally run:

```bash
jupyter-lab
```

to launch the IDE.