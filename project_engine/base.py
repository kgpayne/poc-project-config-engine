from typing import List
from dataclasses import dataclass


@dataclass
class ConfigBase:
    name: str


@dataclass
class ExtractorConfig(ConfigBase):
    variant: str = 'meltano'


@dataclass
class LoaderConfig(ConfigBase):
    pass


@dataclass
class ScheduleConfig(ConfigBase):
    pass
