import glob
import logging
from pathlib import Path
from datafiles import datafile
from datafiles.model import create_model
from dataclasses import dataclass, field
from typing import List, Union

from .base import ConfigBase, ExtractorConfig, LoaderConfig, ScheduleConfig

logger = logging.getLogger(__name__)


@dataclass
class Plugins:
    extractors: List[ExtractorConfig] = field(default_factory=list)
    loaders: List[LoaderConfig] = field(default_factory=list)


@dataclass
class MeltanoFile:
    plugins: Plugins = Plugins()
    schedules: List[ScheduleConfig] = field(default_factory=list)
    include_paths: List[str] = field(default_factory=list)
    version: int = 1


@dataclass
class SubFile:
    plugins: Plugins = Plugins()
    schedules: List[ScheduleConfig] = field(default_factory=list)

    @property
    def name(self):
        return Path(self.datafile.path).stem  # TODO: using stem as the `--store` name means subfile names must be globally unique


@dataclass
class IndexCard:
    config: ConfigBase
    file: Union[MeltanoFile, SubFile]
    position: int


class ProjectConfig:

    def __init__(self, root_dir: str):
        self.root = Path(root_dir).resolve()
        self._meltano_file_path = self.root / 'meltano.yaml'
        self._meltano_file = None
        self._subfiles = []
        self._index = {}
        # run discovery
        self._discover_project_files()
        self._index_project_files()

    def _is_valid_include_path(self, file_path: Path) -> bool:
        """Determine if given path is a valid `include_paths` file."""
        # Verify path is a file
        if not (file_path.is_file() and file_path.exists()):
            logger.critical(
                f"Included path '{file_path}' not found."
            )
            raise Exception("Invalid Included Path")
        # checks passed
        return True

    def _resolve_include_paths(self, include_path_patterns: List[str]) -> List[Path]:
        """Return a list of paths from a list of glob pattern strings."""
        include_paths = []
        if include_path_patterns:
            for pattern in include_path_patterns:
                for path_name in glob.iglob(str(self.root / pattern)):
                    path = Path(path_name)
                    if self._is_valid_include_path(path):
                        include_paths.append(path)
            # never include meltano.yml
            if self._meltano_file_path in include_paths:
                include_paths.remove(self._meltano_file_path)
        return include_paths

    def _discover_project_files(self):
        """Find all config files."""
        if self._meltano_file_path.exists():
            self._meltano_file = create_model(MeltanoFile, pattern=str(self._meltano_file_path), manual=True)()
        else:
            raise FileNotFoundError(f"File not found: '{self._meltano_file_path}'")
        # find subfiles
        if self.meltano_file.include_paths:
            subfile_paths = self._resolve_include_paths(self.meltano_file.include_paths)
            for path in subfile_paths:
                self._subfiles.append(
                    create_model(SubFile, pattern=str(path), manual=True)()
                )

    def _index_extractors(self):
        self._index['extractors'] = {}
        if self.meltano_file.plugins.extractors:
            for index, extractor in enumerate(self.meltano_file.plugins.extractors):
                self._index['extractors'][extractor.name] = IndexCard(config=extractor, file=self.meltano_file, position=index)
        if self.subfiles:
            for subfile in self.subfiles:
                if subfile.plugins.extractors:
                    for index, extractor in enumerate(subfile.plugins.extractors):
                        self._index['extractors'][extractor.name] = IndexCard(config=extractor, file=subfile, position=index)

    def _index_project_files(self):
        self._index_extractors()

    @property
    def meltano_file(self):
        return self._meltano_file

    @property
    def subfiles(self):
        return self._subfiles

    @property
    def extractors(self):
        return [
            idx.config for _, idx in self._index['extractors'].items()
        ]

    def get_subfile(self, name):
        return next(iter([sbf for sbf in self.subfiles if sbf.name == name]), None)

    def get_extractor(self, name):
        idx = self._index['extractors'].get(name)
        if idx:
            return idx.config
        else:
            raise KeyError(f"Extractor with name '{name}' not found.")

    def update_extractor(self, extractor):
        index_card = self._index['extractors'].get(extractor.name)
        if index_card is None:
            raise KeyError(f"Plugin with name {extractor.name} not found.")
        index_card.file.plugins.extractors[index_card.position] = extractor
        index_card.file.datafile.save()
        self._index_project_files()  # TODO: only reindex specific file
        return self.get_extractor(extractor.name)

    def add_extractor(self, extractor, store=None):
        # get relevant subfile
        if store and store != 'meltano_yml':
            target_file = self.get_subfile(store)
        else:
            target_file = self.meltano_file
        # add extractor
        if extractor.name in self._index['extractors'].keys():
            raise KeyError(f"Extractor with name '{extractor.name}' already exists in project.")
        target_file.plugins.extractors.append(extractor)
        target_file.datafile.save()
        self._index_project_files()  # TODO: only reindex specific file
        return self.get_extractor(extractor.name)

    def remove_extractor(self, extractor):
        index_card = self._index['extractors'].get(extractor.name)
        if index_card is None:
            raise KeyError(f"Plugin with name {extractor.name} not found.")
        index_card.file.plugins.extractors.remove(index_card.config)
        index_card.file.datafile.save()
        self._index_project_files()  # TODO: only reindex specific file
